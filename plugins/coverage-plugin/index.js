var MyTabModel = Backbone.Collection.extend({
    url: 'data/myplugindata.json'
});

class MyLayout extends allure.components.AppLayout {

    initialize() {
        this.model = new MyTabModel();
    }

    loadData() {
        return this.model.fetch();
    }

    getContentView() {
        return new MyView({items: this.model.models});
    }
}
const template = function (data) {
    html = '<h2 class="pane__title">Coverage</h2>';
    for (var item of data.items) {
        html += '<h3>' + item.attributes.type + ': ' + item.attributes.count + '/' + item.attributes.total + ' (' + item.attributes.percentage + '%)</h3>';
    }
    return html;
};

var MyView = Backbone.Marionette.View.extend({
    template: template,

    render: function () {
        this.$el.html(this.template(this.options));
        return this;
    }
});

allure.api.addTab('mytab', {
    title: 'Coverage', icon: 'fa fa-trophy',
    route: 'mytab',
    onEnter: (function () {
        return new MyLayout();
    })
});